$(function() {
    // flexslider
    $('.flexslider').flexslider();

    // fixed nav
    var $nav = $('.navbar');
    var $header = $('.header');
    var headerHeight = $header.height();
    var navHeight = $nav.height();
    var fixedClass = 'navbar-fixed-top';

    $(window).scroll(function() {
        var scrollTop = $(this).scrollTop();

        if (scrollTop > headerHeight) {
            $nav.addClass(fixedClass);
            $header.css('margin-bottom', navHeight);
        } else {
            $nav.removeClass(fixedClass);
            $header.css('margin-bottom', 0);
        }
    });

    // parallax
    var touch = Modernizr.touch;
    $('.parallax').imageScroll({
        coverRatio: 0.5,
        touch: touch
    });
});